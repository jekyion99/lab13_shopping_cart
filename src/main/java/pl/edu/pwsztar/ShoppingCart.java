package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart implements ShoppingCartOperation {
    private ArrayList<Product> cart = new ArrayList<Product>();

    public boolean addProducts(String productName, int price, int quantity) {
        Product product = new Product(productName, price, quantity);
        int productID = findSameProduct(productName);

        if(price >= 0 &&  quantity > 0 && cart.size() < PRODUCTS_LIMIT) {
            if (productID >= 0 && cart.get(productID).getProductName() == productName && cart.get(productID).getPrice() == price) {
                cart.get(productID).setQuantity(cart.get(productID).getQuantity() + quantity);
                return true;
            } else if (productID >= 0 && cart.get(productID).getProductName() == productName && cart.get(productID).getPrice() != price) {
                return false;
            } else {
                cart.add(product);
                return true;
            }
        } else {
            return false;
        }
    }

    public boolean deleteProducts(String productName, int quantity) {
        int productID = findSameProduct(productName);
        if(productID == WRONG_PRODUCT){
            return false;
        }else {
            if (cart.get(productID).getQuantity() == quantity) {
                cart.remove(productID);
                return true;
            } else if(cart.get(productID).getQuantity() > quantity){
                cart.get(productID).setQuantity(cart.get(productID).getQuantity() - quantity);
                return true;
            }else {
                return false;
            }
        }

    }

    public int getQuantityOfProduct(String productName) {
        int productID = findSameProduct(productName);
        if(productID == WRONG_PRODUCT){
            return 0;
        }else {
            return cart.get(productID).getQuantity();
        }
    }

    public int getSumProductsPrices() {
        int sum = 0;
        for(int i = 0; i < cart.size(); i++){
            sum += cart.get(i).getPrice() * cart.get(i).getQuantity();
        }
        return sum;
    }

    public int getProductPrice(String productName) {
        int productID = findSameProduct(productName);
        return cart.get(productID).getPrice();

    }

    public List<String> getProductsNames() {
        List<String> productsNames = new ArrayList<String>();
        for(int i = 0; i < cart.size(); i++){
            productsNames.add(cart.get(i).getProductName());
        }
        return productsNames;
    }

    public int findSameProduct(String productName){
        int productID = WRONG_PRODUCT;
        for(int i = 0; i < cart.size(); i++){
            if(cart.get(i).getProductName() == productName){
                productID = i;
            }
        }
        return productID;
    }

}
